import { HttpClient } from '@angular/common/http';
import { CompileTemplateMetadata } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Company } from '../Model/Company';
import { Stock } from '../Model/Stock';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  constructor(private _httpClient: HttpClient) { }


  getCompanies(){
    return this._httpClient
            .get<Company[]>(environment.baseUrl +'/companyapi/api/v1.0/market/company/getall')
            .pipe(
              map(company=>{
                return company.map(comp=>{
                   return comp;
                })
              }),
              tap(resp=>{
                return resp;
              }),
              catchError(this.handleError)
            );
  }

  getStocks(compcode:number, startdate:Date,enddate:Date){
    return this._httpClient.get<Stock[]>(environment.baseUrl +'/stockapi/api/v1.0/market/stock/get/'
                                        + compcode + '/' + startdate + '/' + enddate)
                                        .pipe(
                                          catchError(this.handleError)
                                        );

  }

  handleError(error){
   let errorMessage = '';
   if (error.error instanceof ErrorEvent) {
     // client-side error
     errorMessage = `Error: ${error.error.message}`;

   } else {
     // server-side error
     errorMessage = `Error Code: ${error.status} Message: ${error.message}`;
   }
   return throwError(errorMessage);
  }

}
