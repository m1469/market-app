export class Company{
    companyCode:number;
    companyName:string;
    companyCEO:string;
    companyTurnover:number;
    companyWebsite:string;
    stockExchange:string;    
}