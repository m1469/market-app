import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Company } from './Model/Company';
import { Stock } from './Model/Stock';
import { CompanyService } from './Service/company.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'market-app';
  companies: Company[] = [];
  compstocks: Stock[] = [];
  mycompsub: Subscription;
  mystocksub: Subscription;
  startdate: Date;
  enddate: Date;
  companyval: number;
  searching: boolean = false;
  isloading: boolean = false;
  error: string = "";
  minstock: number;
  maxstock: number;
  avgstock: number;

  constructor(private _compservice: CompanyService) { }

  ngOnInit() {
    this.isloading = true;
    this.mycompsub = this._compservice.getCompanies().subscribe(comp => {
      this.companies = comp;
      this.isloading = false;
    },
      error => {
        this.error = error;
        this.isloading = false;
      });
  }

  searchrecords() {
    console.log(this.startdate);
    console.log(this.enddate);
    console.log(this.companyval);
    this.error = "";
    this.searching = true;
    this.isloading = true;
    this.mystocksub = this._compservice.getStocks(this.companyval, this.startdate, this.enddate).subscribe(stck => {
      console.log(stck);
      this.compstocks = stck;
      if (stck != null && stck.length > 0) 
      {
        let myobj = stck.reduce((accumulator, curval) => {
          if(accumulator.min == 0)
            accumulator.min = curval.price;

          accumulator.min > curval.price ? accumulator.min = curval.price : accumulator.min;
          accumulator.max < curval.price ? accumulator.max = curval.price : accumulator.max;
          accumulator.avg += curval.price;
          return accumulator;
        }, { min: 0, max: 0, avg: 0 });
        myobj.avg = myobj.avg / stck.length;
        console.log(myobj);
        this.minstock = myobj.min;
        this.maxstock = myobj.max;
        this.avgstock = myobj.avg;
      }
      this.isloading = false;
    },
      error => {
        this.error = error;
        this.isloading = false;
      });

  }

  ngOnDestroy() {
    this.mycompsub.unsubscribe();
    this.mystocksub.unsubscribe();
  }


}
