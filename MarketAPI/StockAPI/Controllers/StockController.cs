﻿using Microsoft.AspNetCore.Mvc;
using StockAPI.Models;
using StockAPI.Services;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace StockAPI.Controllers
{
    [Route("api/v{version:apiVersion}/market/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class StockController : ControllerBase
    {
        private IStockService _service;
        public StockController(IStockService service)
        {
            _service = service;
        }

        // GET api/<StockController>/5
        [HttpGet("get/{companycode}/{startdate}/{enddate}")]
        public IActionResult Get(int companycode, string startdate, string enddate)
        {
            try
            {
                var stocklist = _service.ViewStock(startdate, enddate, companycode);
                return Ok(stocklist);
            }
            catch (Exception ex)
            {
                return BadRequest("There is an error while fetching stock list.");
            }
        }

        // POST api/<StockController>
        [HttpPost("add/{companycode}")]
        public IActionResult Post([FromBody]StockVM stock, int companycode)
        {
            try
            {
                var _stock = _service.AddStock(stock, companycode);
                return Created("New stocks Created for ", _stock);
            }
            catch (Exception ex)
            {
                return BadRequest("There is an error while adding stock with company code " + companycode + ".");
            }
        }

    }
}
