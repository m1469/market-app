﻿using MongoDB.Driver;
using StockAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;

namespace StockAPI.Services
{
    public class StockService : IStockService
    {
        private readonly IMongoCollection<Stock> _stock;
        public StockService(ICompanyDBSetting setting)
        {
            MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(setting.ConnectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            var client = new MongoClient(settings);

            var database = client.GetDatabase(setting.Database);

            _stock = database.GetCollection<Stock>(setting.StockCollection);
        }

        public Stock AddStock(StockVM stock, int companycode)
        {
            var _newstock = new Stock()
            {
                CompanyCode = companycode,
                Price = stock.Price,
                PriceDate = DateTime.UtcNow
            };
            _stock.InsertOne(_newstock);
            return _newstock;
        }

        public List<Stock> ViewStock(string startdate, string enddate, int companycode)
        {
            DateTime startdt;
            DateTime enddt;
            if (DateTime.TryParse(startdate, out startdt) && DateTime.TryParse(enddate, out enddt))
            {
                List<Stock> lststock = _stock.Find(s => (s.CompanyCode == companycode)
                                                    && (s.PriceDate > startdt
                                                    &&  s.PriceDate < enddt))
                                                .ToList();
                return lststock;
            }
            return null;
        }
    }
}
