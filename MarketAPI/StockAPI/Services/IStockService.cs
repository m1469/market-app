﻿using StockAPI.Models;
using System.Collections.Generic;

namespace StockAPI.Services
{
    public interface IStockService
    {
        Stock AddStock(StockVM stock, int companycode);

        List<Stock> ViewStock(string startdate, string enddate, int companycode);
    }
}
