﻿using System.ComponentModel.DataAnnotations;

namespace StockAPI.Models
{
    public class StockVM
    {
        [Required]
        public decimal Price { get; set; }
    }
}
