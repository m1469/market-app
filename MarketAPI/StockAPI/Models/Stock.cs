﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace StockAPI.Models
{
    public class Stock
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? Id { get; set; }
        
        [Required]
        public int CompanyCode { get; set; }

        [Required]
        public decimal Price { get; set; }

        public DateTime PriceDate { get; set; }
    }
}
