﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StockAPI.Models
{
    public class CompanyDBSetting : ICompanyDBSetting
    {
        public string CompCollection { get; set; }
        public string StockCollection { get; set; }
        public string ConnectionString { get; set; }
        public string Database { get; set; }
    }

    public interface ICompanyDBSetting
    {
        string CompCollection { get; set; }
        string StockCollection { get; set; }
        string ConnectionString { get; set; }
        string Database { get; set; }
    }
}
