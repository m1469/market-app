﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MarketAPI.Models
{
    public class CompanyVM
    {
        [Required(ErrorMessage = "Company Code is required")]
        [Range(1, 1000)]
        public int CompanyCode { get; set; }

        [Required(ErrorMessage = "CompanyName is required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "CompanyCEO is required")]
        public string CompanyCEO { get; set; }

        [Required(ErrorMessage = "CompanyTurnover is required")]
        [Range(100000000, int.MaxValue, ErrorMessage = "CompanyTurnover must be greater than 10 cr")]
        public int CompanyTurnover { get; set; }

        [Required(ErrorMessage = "CompanyWebsite is required")]
        public string CompanyWebsite { get; set; }

        [Required(ErrorMessage = "StockExchange is required")]
        public string StockExchange { get; set; }
    }
}
