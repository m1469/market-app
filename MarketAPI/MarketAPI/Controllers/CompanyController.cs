﻿using MarketAPI.Models;
using MarketAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MarketAPI.Controllers
{
    [Route("api/v{version:apiVersion}/market/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    public class CompanyController : ControllerBase
    {
        private ICompanyService _service;
        public CompanyController(ICompanyService service)
        {
            _service = service;
        }

        [HttpGet("getall")]
        public IActionResult Get()
        {
            try
            {
                var complist = _service.GetAllCompany();
                return Ok(complist);
            }
            catch (Exception ex)
            {
                return BadRequest( "There is an error while fetching company list.");
            }
        }

        // GET api/<CompanyController>/5
        [HttpGet("info/{companycodeid}")]
        public IActionResult GetCompanyByID(int companycodeid)
        {
            try
            {
                var complist = _service.GetCompanyByCode(companycodeid);
                return Ok(complist);
            }
            catch (Exception ex)
            {
                return BadRequest("There is an error while fetching company with id " + companycodeid + ".");
            }
        }

        // POST api/<CompanyController>
        [HttpPost("register")]
        public IActionResult Post([FromBody]CompanyVM company)
        {
            try
            {
                var comp = _service.CreateCompany(company);
                return Created("New Company Created", comp);
            }
            catch (Exception ex)
            {
                return BadRequest("There is an error while adding company with name " + company.CompanyName + ".");
            }
        }

        // DELETE api/<CompanyController>/5
        [HttpDelete("delete/{companycode}")]
        public IActionResult Delete(int companycode)
        {
            try
            {
                _service.DeleteCompany(companycode);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest("There is an error while deleting company with id " + companycode + ".");
            }
        }
    }
}
