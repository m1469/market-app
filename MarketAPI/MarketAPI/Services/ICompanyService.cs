﻿using MarketAPI.Models;
using System.Collections.Generic;

namespace MarketAPI.Services
{
    public interface ICompanyService
    {
        List<Company> GetAllCompany();
        Company GetCompanyByCode(int companycode);
        Company CreateCompany(CompanyVM company);
        void DeleteCompany(int companycode);
    }
}