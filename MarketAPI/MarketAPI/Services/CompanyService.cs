﻿using MarketAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace MarketAPI.Services
{
    public class CompanyService : ICompanyService
    {
        private readonly IMongoCollection<Company> _company;
        public CompanyService(ICompanyDBSetting setting)
        {
            MongoClientSettings settings = MongoClientSettings.FromUrl(new MongoUrl(setting.ConnectionString));
            settings.SslSettings = new SslSettings() { EnabledSslProtocols = SslProtocols.Tls12 };
            var client = new MongoClient(settings);

            var database = client.GetDatabase(setting.Database);

            _company = database.GetCollection<Company>(setting.CompCollection);            
        }

        public List<Company> GetAllCompany()
        {
            return _company.Find(com => true).ToList();
        }

        public Company GetCompanyByCode(int companycode)
        {
            return _company.Find(com => com.CompanyCode == companycode).FirstOrDefault();
        }

        public Company CreateCompany(CompanyVM company)
        {
            if (GetCompanyByCode(company.CompanyCode) != null)
                return null;

            var _newcomp = new Company()
            {
                CompanyCEO = company.CompanyCEO,
                CompanyCode = company.CompanyCode,
                CompanyName = company.CompanyName,
                CompanyTurnover = company.CompanyTurnover,
                CompanyWebsite = company.CompanyWebsite
            };
            _company.InsertOne(_newcomp);
            return _newcomp;
        }

        public void DeleteCompany(int companycode)
        {
            _company.DeleteOne(com => com.CompanyCode == companycode);
        }
    }
}
